<?php

use Smorken\Redactor\Types\Exception;
use Smorken\Redactor\Types\RegEx;

return [
    'active' => true,
    'types' => [
        RegEx::class => [
            [
                ['/.*pass.*/i', '/"pass(.*?)"\s*\:\s*"(.*?)"/i', '/.*key.*/i', '/.*salt.*/i', '/.*token.*/i'],
            ],
        ],
        Exception::class => [
            [
                '*::authenticate',
                [1],
            ],
            [
                '*::bind',
                [1],
            ],
            [
                '*::connect',
                [],
            ],
            [
                '*::login',
                [],
            ],
            [
                '*::setKey',
                [],
            ],
            [
                '*::setSalt',
                [],
            ],
            [
                '*::setPassword',
                [],
            ],
            [
                'PDO::__construct',
                [],
            ],
            [
                '*::createPdoConnection',
                [],
            ],
            [
                '*::createConnection',
                [],
            ],
            [
                '*::doAuthentication',
                [1],
            ],
            [
                '*::validateCredentials',
                [],
            ],
            [
                'SessionGuard::hasValidCredentials',
                [],
            ],
            [
                'SessionGuard::attempt',
                [],
            ],
        ],
    ],
];
