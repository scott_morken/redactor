<?php

namespace Smorken\Redactor\Types;

use Smorken\Redactor\Contracts\Types\Base;
use Smorken\Redactor\Contracts\Withs\Base as RedactWith;
use Smorken\Redactor\Handlers\ExceptionHandler;
use Smorken\Redactor\Handlers\IterableHandler;
use Smorken\Redactor\Handlers\ObjectHandler;
use Smorken\Redactor\Handlers\ScalarHandler;
use Smorken\Redactor\Withs\SimpleString;

abstract class AbstractType implements Base
{
    protected array $default_handlers = [
        self::EXCEPTION_HANDLER => ExceptionHandler::class,
        self::ITERABLE_HANDLER => IterableHandler::class,
        self::OBJECT_HANDLER => ObjectHandler::class,
        self::SCALAR_HANDLER => ScalarHandler::class,
    ];

    protected array $handlers = [
        self::EXCEPTION_HANDLER => null,
        self::ITERABLE_HANDLER => null,
        self::OBJECT_HANDLER => null,
        self::SCALAR_HANDLER => null,
    ];

    protected bool $redacted = false;

    protected ?\Smorken\Redactor\Contracts\Withs\Base $with = null;

    public function __construct(?RedactWith $with = null)
    {
        if (! is_null($with)) {
            $this->setWith($with);
        }
    }

    public function findAndRedact(mixed $input): mixed
    {
        if (is_object($input)) {
            return $this->redactObject($input);
        }
        if (is_iterable($input)) {
            return $this->redactIterable($input);
        }
        if (is_scalar($input)) {
            return $this->redactScalar($input);
        }

        return $input;
    }

    public function getHandler(string $type): \Smorken\Redactor\Contracts\Handlers\Base
    {
        if (is_null($this->handlers[$type]) && isset($this->default_handlers[$type])) {
            $this->setHandler($type, new $this->default_handlers[$type]($this));
        }
        if ($this->handlers[$type]) {
            return $this->handlers[$type];
        }
        throw new \InvalidArgumentException("$type is not a valid handler type.");
    }

    /**
     * @return string
     */
    public function redact(mixed $input, bool $on_key = true): mixed
    {
        return $this->getWith()
            ->redact($input);
    }

    public function redacted(): bool
    {
        return $this->redacted;
    }

    public function setHandler(string $type, \Smorken\Redactor\Contracts\Handlers\Base $handler): void
    {
        $this->handlers[$type] = $handler;
    }

    protected function getWith(): RedactWith
    {
        if (! $this->with) {
            $this->setWith(new SimpleString);
        }

        return $this->with;
    }

    public function setWith(RedactWith $redactWith): void
    {
        $this->with = $redactWith;
    }

    protected function redactIterable(iterable $input): iterable
    {
        return $this->getHandler(self::ITERABLE_HANDLER)
            ->handle($input);
    }

    protected function redactObject(object $input): object
    {
        if ($input instanceof \Throwable) {
            return $this->getHandler(self::EXCEPTION_HANDLER)
                ->handle($input);
        }

        return $this->getHandler(self::OBJECT_HANDLER)
            ->handle($input);
    }

    protected function redactScalar(string $input): string
    {
        return $this->getHandler(self::SCALAR_HANDLER)
            ->handle($input);
    }

    protected function reset(): void
    {
        $this->redacted = false;
    }
}
