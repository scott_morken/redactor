<?php

namespace Smorken\Redactor\Types;

class SimpleString extends AbstractMatchType implements \Smorken\Redactor\Contracts\Types\SimpleString
{
    public function shouldRedact(mixed $input): bool
    {
        $should = in_array($input, $this->matches, true);
        if ($should) {
            $this->redacted = true;
        }

        return $should;
    }
}
