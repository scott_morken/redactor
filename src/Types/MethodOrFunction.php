<?php

namespace Smorken\Redactor\Types;

use Smorken\Redactor\Contracts\Withs\Base as RedactWith;

class MethodOrFunction extends AbstractType implements \Smorken\Redactor\Contracts\Types\MethodOrFunction
{
    use \Smorken\Redactor\Types\Traits\RegEx;

    protected array $matches = [];

    protected string $name;

    protected array $positions = [];

    public function __construct(string $name, array $positions = [0], ?RedactWith $with = null)
    {
        $this->setName($name);
        $this->setParamPositions($positions);
        parent::__construct($with);
    }

    public function redact(mixed $input, bool $on_key = true): mixed
    {
        if ($on_key) {
            return $this->getWith()
                ->redact($input);
        }

        return $this->redactInInput($input);
    }

    public function setMatches(array $matches = []): void
    {
        $this->matches = $matches;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
        $this->buildMatch($name);
    }

    public function setParamPositions(array $positions = [0]): void
    {
        $this->positions = $positions;
    }

    protected function buildMatch(string $function_name): void
    {
        $regex = sprintf("/%s\s*\(/i", preg_quote($function_name));
        $this->setMatches([$regex]);
    }

    protected function findClosingParen(mixed $input, int $start_pos): int
    {
        $next_close = strpos((string) $input, ')', $start_pos + 1);
        $next_open = strpos((string) $input, '(', $start_pos + 1);
        if ($next_open === false) {
            return $next_close;
        }
        if ($next_close !== false && $next_open > $start_pos && $next_open < $next_close) {
            return $this->findClosingParen($input, $next_close);
        }

        return $next_close;
    }

    protected function redactArgString(string $argstr): string
    {
        $args = array_map('trim', explode(',', $argstr));
        $redacted = $this->redactParams($args);

        return $redacted;
    }

    protected function redactFunction(mixed $input, int $starting_pos): string
    {
        $open_paren = strpos((string) $input, '(', $starting_pos);
        if ($open_paren === false) {
            return $input;
        }
        $close_paren = $this->findClosingParen($input, $open_paren);
        $argstr = substr((string) $input, $open_paren + 1, $close_paren - $open_paren - 1);
        $redacted = $this->redactArgString($argstr);

        return substr((string) $input, 0, $open_paren + 1).$redacted.substr((string) $input, $close_paren);
    }

    protected function redactInInput(mixed $input, int $offset = 0): string
    {
        if ($input && strlen((string) $input) >= $offset) {
            $pos = strpos((string) $input, $this->name, $offset);
            if ($pos !== false) {
                $input = $this->redactInInput($this->redactFunction($input, $pos), $pos + 1);
            }
        }

        return $input;
    }

    protected function redactParams(array $args): string
    {
        $redacted = [];
        foreach ($args as $i => $arg) {
            if (empty($this->positions) || in_array($i, $this->positions)) {
                $redacted[$i] = $this->getWith()
                    ->redact($arg);
            } else {
                $redacted[$i] = $arg;
            }
        }

        return implode(', ', $redacted);
    }
}
