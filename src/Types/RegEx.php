<?php

namespace Smorken\Redactor\Types;

class RegEx extends AbstractMatchType implements \Smorken\Redactor\Contracts\Types\RegEx
{
    use \Smorken\Redactor\Types\Traits\RegEx;
}
