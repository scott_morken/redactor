<?php

namespace Smorken\Redactor\Types\Traits;

trait RegEx
{
    protected ?string $match = null;

    public function redact(mixed $input, bool $on_key = true): mixed
    {
        if ($on_key) {
            return $this->getWith()
                ->redact($input);
        }

        return preg_replace($this->match, $this->getWith()
            ->redact($input), $input);
    }

    public function shouldRedact(mixed $input): bool
    {
        if (is_scalar($input)) {
            foreach ($this->matches as $i => $regex) {
                if (preg_match($regex, $input)) {
                    $this->match = $regex;
                    $this->redacted = true;

                    return true;
                }
            }
        }

        return false;
    }
}
