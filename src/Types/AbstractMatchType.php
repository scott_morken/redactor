<?php

namespace Smorken\Redactor\Types;

use Smorken\Redactor\Contracts\Withs\Base as RedactWith;

abstract class AbstractMatchType extends AbstractType
{
    protected array $matches = [];

    public function __construct(array $matches = [], ?RedactWith $redactWith = null)
    {
        $this->setMatches($matches);
        parent::__construct($redactWith);
    }

    public function setMatches(array $matches = []): void
    {
        $this->matches = $matches;
    }
}
