<?php

namespace Smorken\Redactor\Types;

use Smorken\Redactor\Contracts\Withs\Base as RedactWith;

class Exception extends AbstractType implements \Smorken\Redactor\Contracts\Types\Exception
{
    protected ?string $class = null;

    protected string $name;

    protected array $positions = [];

    public function __construct(string $full_name, array $param_positions = [0], ?RedactWith $with = null)
    {
        $this->setAllNames($full_name);
        $this->setParamPositions($param_positions);
        parent::__construct($with);
    }

    public function getClass(): ?string
    {
        return $this->class;
    }

    public function setClass(string $name): void
    {
        $this->class = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function redact(mixed $input, bool $on_key = true): mixed
    {
        foreach (($input['args'] ?? []) as $i => $arg) {
            if (empty($this->positions) || in_array($i, $this->positions)) {
                $input['args'][$i] = $this->getWith()
                    ->redact($arg);
            } else {
                $input['args'][$i] = $arg;
            }
        }

        return $input;
    }

    public function setParamPositions(array $positions = [0]): void
    {
        $this->positions = $positions;
    }

    public function shouldRedact(mixed $input): bool
    {
        return $input === 'trace';
    }

    protected function setAllNames(string $full_name): void
    {
        $parts = explode('::', $full_name);
        if (count($parts) === 2) {
            $this->setClass($parts[0]);
            $this->setName($parts[1]);
        } else {
            $this->setName($parts[0]);
        }
    }
}
