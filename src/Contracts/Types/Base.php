<?php

namespace Smorken\Redactor\Contracts\Types;

use Smorken\Redactor\Contracts\Handlers\ExceptionHandler;
use Smorken\Redactor\Contracts\Handlers\IterableHandler;
use Smorken\Redactor\Contracts\Handlers\ObjectHandler;
use Smorken\Redactor\Contracts\Handlers\ScalarHandler;
use Smorken\Redactor\Contracts\Withs\Base as RedactWith;

interface Base
{
    public const EXCEPTION_HANDLER = ExceptionHandler::class;

    public const ITERABLE_HANDLER = IterableHandler::class;

    public const OBJECT_HANDLER = ObjectHandler::class;

    public const SCALAR_HANDLER = ScalarHandler::class;

    public function findAndRedact(mixed $input): mixed;

    public function getHandler(string $type): \Smorken\Redactor\Contracts\Handlers\Base;

    public function redact(mixed $input, bool $on_key = true): mixed;

    public function redacted(): bool;

    public function setHandler(string $type, \Smorken\Redactor\Contracts\Handlers\Base $handler): void;

    public function setWith(RedactWith $redactWith): void;

    public function shouldRedact(mixed $input): bool;
}
