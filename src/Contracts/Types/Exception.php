<?php

namespace Smorken\Redactor\Contracts\Types;

interface Exception extends Base
{
    public function getClass(): ?string;

    public function getName(): string;

    public function setClass(string $name): void;

    public function setName(string $name): void;

    public function setParamPositions(array $positions = [0]): void;
}
