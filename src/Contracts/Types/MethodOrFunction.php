<?php

namespace Smorken\Redactor\Contracts\Types;

interface MethodOrFunction extends Matches
{
    public function setName(string $name): void;

    public function setParamPositions(array $positions = [0]): void;
}
