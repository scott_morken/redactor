<?php

namespace Smorken\Redactor\Contracts\Types;

interface Matches extends Base
{
    public function setMatches(array $matches = []): void;
}
