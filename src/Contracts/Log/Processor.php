<?php

namespace Smorken\Redactor\Contracts\Log;

use Smorken\Redactor\Contracts\Redactor;

interface Processor
{
    public function getRedactor(): Redactor;

    public function setRedactor(Redactor $redactor): void;
}
