<?php

namespace Smorken\Redactor\Contracts;

use Smorken\Redactor\Contracts\Types\Base;

interface Redactor
{
    /**
     * @throws \Smorken\Redactor\Exceptions\TypeInstanceException
     */
    public function addType(Base $type);

    public function findAndRedact(mixed $input): mixed;

    /**
     * @return \Smorken\Redactor\Contracts\Types\Base[]
     */
    public function getTypes(): iterable;

    public function isActive(): bool;

    public function setActive(bool $active): void;

    public function setTypes(array $types): void;
}
