<?php

namespace Smorken\Redactor\Contracts\Handlers;

interface Base
{
    public function getType(): \Smorken\Redactor\Contracts\Types\Base;

    public function handle(mixed $input): mixed;
}
