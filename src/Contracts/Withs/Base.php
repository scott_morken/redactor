<?php

namespace Smorken\Redactor\Contracts\Withs;

interface Base
{
    public function redact(mixed $input): string;
}
