<?php

namespace Smorken\Redactor\Contracts\Withs;

interface SimpleString extends Base
{
    public function setSimpleString(string $simpleString): void;
}
