<?php

namespace Smorken\Redactor\Contracts\Withs;

interface Hash extends Base
{
    public function getKey(): string;
}
