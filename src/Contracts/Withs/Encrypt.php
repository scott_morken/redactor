<?php

namespace Smorken\Redactor\Contracts\Withs;

interface Encrypt extends Base
{
    public function encrypt(string $message, string $key): string;

    public function decrypt(string $encrypted, string $key): string;

    public function getKey(): string;
}
