<?php

namespace Smorken\Redactor\Handlers;

class ScalarHandler extends AbstractHandler implements \Smorken\Redactor\Contracts\Handlers\ScalarHandler
{
    public function handle(mixed $input): mixed
    {
        if ($this->getType()
            ->shouldRedact($input)) {
            return $this->getType()
                ->redact($input, false);
        }

        return $input;
    }
}
