<?php

namespace Smorken\Redactor\Handlers;

use PHPUnit\Framework\TestCase;

class ObjectHandler extends AbstractHandler implements \Smorken\Redactor\Contracts\Handlers\ObjectHandler
{
    protected array $reflected = [];

    protected array $skip_classes = [
        TestCase::class,
    ];

    protected array $skip_properties = [
        'message',
        'class',
        'name',
        'file',
        'code',
        'line',
        'function',
    ];

    public function handle(mixed $input): mixed
    {
        $ref = new \ReflectionClass($input);

        return $this->redactReflected($input, $ref);
    }

    protected function collectReflectedProperties(\ReflectionClass $reflectionClass): array
    {
        $props = [];
        $original = $reflectionClass;
        while ($parent = $reflectionClass->getParentClass()) {
            foreach ($parent->getProperties() as $p) {
                $props[$p->getName()] = $p;
            }
            $reflectionClass = $parent;
        }
        foreach ($original->getProperties() as $p) {
            $props[$p->getName()] = $p;
        }

        return $props;
    }

    protected function handleProperty(object $obj, \ReflectionProperty $property): void
    {
        if (in_array($property->getName(), $this->skip_properties)) {
            return;
        }
        $property->setAccessible(true);
        $value = $property->getValue($obj);
        if ($this->getType()->shouldRedact($property->getName())) {
            $property->setValue($obj, $this->getType()->redact($value));
        } else {
            $property->setValue($obj, $this->getType()->findAndRedact($value));
        }
    }

    protected function redactReflected(object $obj, \ReflectionClass $reflectionClass): object
    {
        if ($this->shouldReflectInstance($obj)) {
            foreach ($this->collectReflectedProperties($reflectionClass) as $name => $property) {
                $this->handleProperty($obj, $property);
            }
        }

        return $obj;
    }

    protected function shouldReflectInstance(object $obj): bool
    {
        $class_name = $obj::class;
        foreach ($this->skip_classes as $skip_class) {
            if ($obj instanceof $skip_class) {
                return false;
            }
        }
        if (isset($this->reflected[$class_name])) {
            foreach ($this->reflected[$class_name] as $inst) {
                if ($inst === $obj) {
                    return false;
                }
            }
        } else {
            $this->reflected[$class_name] = [];
        }
        $this->reflected[$class_name][] = $obj;

        return true;
    }
}
