<?php

namespace Smorken\Redactor\Handlers;

class ExceptionHandler extends ObjectHandler implements \Smorken\Redactor\Contracts\Handlers\ExceptionHandler
{
    protected function checkClassMatch(array $value): bool
    {
        return $this->typeClassIsStar() || $this->typeClassEndsWith($value['class'] ?? null);
    }

    protected function checkCorrectStackTrace(array $value): bool
    {
        return $this->checkClassMatch($value) && $this->checkFunctionMatch($value);
    }

    protected function checkFunctionMatch(array $value): bool
    {
        return $this->typeNameEndsWith($value['function'] ?? null);
    }

    protected function endsWith(string $haystack, string $needle): bool
    {
        return str_ends_with($haystack, $needle);
    }

    protected function handleProperty(object $obj, \ReflectionProperty $property): void
    {
        if (in_array($property->getName(), $this->skip_properties)) {
            return;
        }
        $property->setAccessible(true);
        $value = $property->getValue($obj);
        if ($obj instanceof \Throwable && $this->getType()
            ->shouldRedact($property->getName())) {
            foreach ($value as $i => $trace) {
                if ($this->checkCorrectStackTrace($trace)) {
                    $value[$i] = $this->getType()
                        ->redact($trace);
                }
            }
            $property->setValue($obj, $value);
        }
    }

    protected function typeClassEndsWith(?string $value): bool
    {
        /** @var \Smorken\Redactor\Contracts\Types\Exception $t */
        $t = $this->getType();
        $className = $t->getClass();
        if ($value && $className) {
            return $this->endsWith($value, $className);
        }

        return $value === $className;
    }

    protected function typeClassIsStar(): bool
    {
        /** @var \Smorken\Redactor\Contracts\Types\Exception $t */
        $t = $this->getType();

        return $t->getClass() === '*';
    }

    protected function typeNameEndsWith(?string $value): bool
    {
        /** @var \Smorken\Redactor\Contracts\Types\Exception $t */
        $t = $this->getType();
        $name = $t->getName();
        if ($value && $name) {
            return $this->endsWith($value, $name);
        }

        return $value === $name;
    }
}
