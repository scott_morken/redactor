<?php

namespace Smorken\Redactor\Handlers;

use Smorken\Redactor\Contracts\Handlers\Base;

abstract class AbstractHandler implements Base
{
    public function __construct(protected \Smorken\Redactor\Contracts\Types\Base $type) {}

    public function getType(): \Smorken\Redactor\Contracts\Types\Base
    {
        return $this->type;
    }
}
