<?php

namespace Smorken\Redactor\Handlers;

class IterableHandler extends AbstractHandler
{
    public function handle(mixed $input): mixed
    {
        foreach ($input as $k => $v) {
            if ($this->getType()
                ->shouldRedact($k)) {
                $input[$k] = $this->getType()
                    ->redact($v);
            } elseif ($this->getType()
                ->shouldRedact($v)) {
                $input[$k] = $this->getType()
                    ->redact($v, false);
            } else {
                $input[$k] = $this->getType()
                    ->findAndRedact($v);
            }
        }

        return $input;
    }
}
