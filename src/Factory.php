<?php

namespace Smorken\Redactor;

use Smorken\Redactor\Contracts\Types\Base;

class Factory
{
    public static function fromConfig(array $config): Contracts\Redactor
    {
        $types = [];
        $active = (! isset($config['active']) || (bool) $config['active']);
        $types_config = ($config['types'] ?? []);
        foreach ($types_config as $type_class => $type_params) {
            foreach ($type_params as $params) {
                if ($params instanceof Base) {
                    $types[] = $params;
                } else {
                    try {
                        $types[] = self::createTypeFromArray($type_class, $params);
                    } catch (\Exception $e) {
                        // if we're active, quietly fail... note that this WILL break redaction but
                        // it won't break logging this way
                        if (! $active) {
                            throw $e;
                        }
                    }
                }
            }
        }

        return new \Smorken\Redactor\Redactor($types, $active);
    }

    /**
     * @throws \ReflectionException
     */
    protected static function createTypeFromArray(string $type_class, array $parameters): Base
    {
        $newref = new \ReflectionClass($type_class);
        $instance = $newref->newInstanceArgs($parameters);

        return $instance;
    }
}
