<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/17/17
 * Time: 7:37 AM
 */

namespace Smorken\Redactor;

use Smorken\Redactor\Contracts\Redactor;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    protected static bool $conf_loaded = false;

    /**
     * Bootstrap the application events.
     */
    public function boot(): void
    {
        //$this->bootConfig();
    }

    public function register(): void
    {
        $this->app->bind(Redactor::class, function ($app) {
            $this->bootConfig();
            $config = $app['config']->get('redactor', []);

            return Factory::fromConfig($config);
        });
    }

    protected function bootConfig(): void
    {
        if (! self::$conf_loaded) {
            $config = __DIR__.'/../config/config.php';
            $this->mergeConfigFrom($config, 'redactor');
            // @phpstan-ignore function.notFound
            $this->publishes([$config => config_path('redactor.php')], 'config');
            self::$conf_loaded = true;
        }
    }
}
