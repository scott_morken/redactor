<?php

namespace Smorken\Redactor\Withs;

use Smorken\Redactor\Exceptions\WithEncryptException;

class Encrypt extends AbstractWith implements \Smorken\Redactor\Contracts\Withs\Encrypt
{
    public function __construct(protected string $key, protected int $block_size = 16, protected bool $debug = false) {}

    public function __destruct()
    {
        sodium_memzero($this->key);
    }

    /**
     * @throws \Smorken\Redactor\Exceptions\WithEncryptException|\SodiumException
     */
    public function decrypt(string $encrypted, string $key): string
    {
        if (! $this->debug) {
            throw new WithEncryptException('Debug disabled. Cannot decrypt.');
        }
        $decoded = sodium_hex2bin($encrypted);
        $this->checkDecoded($decoded);
        $nonce = mb_substr($decoded, 0, SODIUM_CRYPTO_SECRETBOX_NONCEBYTES, '8bit');
        $ciphertext = mb_substr($decoded, SODIUM_CRYPTO_SECRETBOX_NONCEBYTES, null, '8bit');
        $plain = $this->checkPlainText(sodium_crypto_secretbox_open($ciphertext, $nonce, $key));
        $plain = $this->checkPlainText(sodium_unpad($plain, $this->getBlockSize()));
        sodium_memzero($ciphertext);
        sodium_memzero($key);

        return $plain;
    }

    /**
     * @throws \SodiumException
     */
    public function encrypt(string $message, string $key): string
    {
        $nonce = random_bytes(SODIUM_CRYPTO_SECRETBOX_NONCEBYTES);
        $message = sodium_pad($message, $this->getBlockSize());
        $cipher = sodium_bin2hex(sprintf('%s%s', $nonce, sodium_crypto_secretbox($message, $nonce, $key)));
        sodium_memzero($message);
        sodium_memzero($key);

        return $cipher;
    }

    /**
     * @throws \Smorken\Redactor\Exceptions\WithEncryptException
     */
    public function getKey(): string
    {
        if (! $this->key) {
            $this->key = sodium_crypto_secretbox_keygen();
        }
        $this->verifyKeyLength($this->key);

        return $this->key;
    }

    /**
     * @throws \Exception
     */
    public function redact(mixed $input): string
    {
        $cipher = $this->encrypt($input, $this->getKey());
        sodium_memzero($input);

        return $cipher;
    }

    /**
     * @throws \Smorken\Redactor\Exceptions\WithEncryptException
     */
    protected function checkDecoded(string|bool $decoded): bool
    {
        if ($decoded === false) {
            throw new WithEncryptException('Decoding failed.');
        }
        if (mb_strlen($decoded, '8bit') < (SODIUM_CRYPTO_SECRETBOX_NONCEBYTES + SODIUM_CRYPTO_SECRETBOX_MACBYTES)) {
            throw new WithEncryptException('The message was truncated.');
        }

        return true;
    }

    protected function checkPlainText(string|bool $plain): string
    {
        if ($plain === false) {
            throw new WithEncryptException('The message was tampered with.');
        }

        return $plain;
    }

    protected function getBlockSize(): int
    {
        return $this->block_size <= 512 ? $this->block_size : 512;
    }

    protected function verifyKeyLength(string $key): void
    {
        if (strlen($key) < SODIUM_CRYPTO_SECRETBOX_KEYBYTES) {
            throw new WithEncryptException(
                sprintf('Key must be at least %d bytes (binary).', SODIUM_CRYPTO_SECRETBOX_KEYBYTES)
            );
        }
    }
}
