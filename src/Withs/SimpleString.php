<?php

namespace Smorken\Redactor\Withs;

class SimpleString extends AbstractWith implements \Smorken\Redactor\Contracts\Withs\SimpleString
{
    protected string $replacement = '[ REDACTED ]';

    public function __construct(?string $replacement = null)
    {
        if (! is_null($replacement)) {
            $this->setSimpleString($replacement);
        }
    }

    public function redact(mixed $input): string
    {
        return $this->replacement;
    }

    public function setSimpleString(string $simpleString): void
    {
        $this->replacement = $simpleString;
    }
}
