<?php

namespace Smorken\Redactor\Withs;

use Smorken\Redactor\Contracts\Withs\Base;

abstract class AbstractWith implements Base {}
