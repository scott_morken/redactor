<?php

namespace Smorken\Redactor\Withs;

use Smorken\Redactor\Exceptions\WithEncryptException;
use Smorken\Redactor\Utils\Random;

class Hash extends AbstractWith implements \Smorken\Redactor\Contracts\Withs\Hash
{
    public function __construct(protected string $key, protected int $hash_length = SODIUM_CRYPTO_GENERICHASH_BYTES) {}

    /**
     * @throws \Smorken\Redactor\Exceptions\WithEncryptException
     */
    public function getKey(): string
    {
        if (! $this->key) {
            $this->key = Random::generate(SODIUM_CRYPTO_GENERICHASH_KEYBYTES);
        }
        $this->verifyKeyLength($this->key);

        return $this->key;
    }

    /**
     * @throws \Smorken\Redactor\Exceptions\WithEncryptException|\SodiumException
     */
    public function redact(mixed $input): string
    {
        return sodium_crypto_generichash((string) $input, $this->getKey());
    }

    protected function getHashLength(): int
    {
        if ($this->hash_length >= SODIUM_CRYPTO_GENERICHASH_BYTES_MIN &&
            $this->hash_length <= SODIUM_CRYPTO_GENERICHASH_BYTES_MAX) {
            return $this->hash_length;
        }

        return SODIUM_CRYPTO_GENERICHASH_BYTES;
    }

    protected function verifyKeyLength(string $key): void
    {
        if (strlen($key) < SODIUM_CRYPTO_GENERICHASH_KEYBYTES_MIN ||
            strlen($key) > SODIUM_CRYPTO_GENERICHASH_KEYBYTES_MAX) {
            throw new WithEncryptException(sprintf('Key must be between %d and %d characters.',
                SODIUM_CRYPTO_GENERICHASH_KEYBYTES_MIN, SODIUM_CRYPTO_GENERICHASH_KEYBYTES_MAX));
        }
    }
}
