<?php

namespace Smorken\Redactor\Utils;

class Random
{
    public static function binary(int $length = 32): string
    {
        return random_bytes($length);
    }

    public static function generate(int $length = 32): string
    {
        return bin2hex(self::binary($length));
    }
}
