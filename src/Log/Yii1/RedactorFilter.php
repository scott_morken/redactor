<?php

namespace Smorken\Redactor\Log\Yii1;

use Smorken\Redactor\Contracts\Log\Processor;
use Smorken\Redactor\Contracts\Redactor;

class RedactorFilter extends \CLogFilter implements \ILogFilter, Processor
{
    /**
     * @var Redactor
     */
    public $redactor;

    /**
     * Filters the given log messages.
     * This is the main method of CLogFilter. It processes the log messages
     * by adding context information, etc.
     *
     * @param  array  $logs  the log messages
     * @return array
     */
    public function filter(&$logs)
    {
        if (! empty($logs)) {
            $logs = $this->getRedactor()
                ->findAndRedact($logs);
            if (($message = $this->getContext()) !== '') {
                array_unshift($logs, [$message, \CLogger::LEVEL_INFO, 'application', YII_BEGIN_TIME]);
            }
            $this->format($logs);
        }

        return $logs;
    }

    public function getRedactor(): Redactor
    {
        return $this->redactor;
    }

    public function setRedactor(Redactor $redactor): void
    {
        $this->redactor = $redactor;
    }

    /**
     * Generates the context information to be logged.
     * The default implementation will dump user information, system variables, etc.
     *
     * @return string the context information. If an empty string, it means no context information.
     */
    protected function getContext()
    {
        $context = [];
        if ($this->logUser && ($user = \Yii::app()
            ->getComponent('user', false)) !== null) {
            $context[] = 'User: '.$user->getName().' (ID: '.$user->getId().')';
        }
        if ($this->dumper === 'var_export' || $this->dumper === 'print_r') {
            foreach ($this->logVars as $name) {
                if (($value = $this->getGlobalsValue($name)) !== null) {
                    $value = $this->getRedactor()
                        ->findAndRedact($value);
                    $context[] = "\${$name}=".call_user_func($this->dumper, $value, true);
                }
            }
        } else {
            foreach ($this->logVars as $name) {
                if (($value = $this->getGlobalsValue($name)) !== null) {
                    $value = $this->getRedactor()
                        ->findAndRedact($value);
                    $context[] = "\${$name}=".call_user_func($this->dumper, $value);
                }
            }
        }

        return implode("\n\n", $context);
    }
}
