<?php

namespace Smorken\Redactor\Log\Monolog;

use Monolog\LogRecord;
use Monolog\Processor\ProcessorInterface;
use Smorken\Redactor\Contracts\Redactor;

class RedactorProcessor implements ProcessorInterface
{
    private Redactor $redactor;

    public function __construct(Redactor $redactor)
    {
        $this->setRedactor($redactor);
    }

    /**
     * @return \Monolog\LogRecord The processed records
     */
    public function __invoke(LogRecord $record): LogRecord
    {
        $redact = ['message', 'context', 'extra'];
        $redacted = [];
        foreach ($redact as $key) {
            $value = $record[$key] ?? null;
            $redacted[$key] = $value ? $this->getRedactor()->findAndRedact($value) : null;
        }

        return $record->with(...$redacted);
    }

    public function getRedactor(): Redactor
    {
        return $this->redactor;
    }

    public function setRedactor(Redactor $redactor): void
    {
        $this->redactor = $redactor;
    }
}
