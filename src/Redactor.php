<?php

namespace Smorken\Redactor;

use Smorken\Redactor\Contracts\Types\Base;

class Redactor implements \Smorken\Redactor\Contracts\Redactor
{
    protected bool $active = true;

    /**
     * @var \Smorken\Redactor\Contracts\Types\Base[]
     */
    protected array $types = [];

    /**
     * Redactor constructor.
     *
     * @param  \Smorken\Redactor\Contracts\Types\Base[]  $types
     */
    public function __construct(array $types, bool $active = true)
    {
        $this->setActive($active);
        $this->setTypes($types);
    }

    public function addType(Base $type): void
    {
        $this->types[] = $type;
    }

    public function findAndRedact(mixed $input): mixed
    {
        if ($this->isActive()) {
            foreach ($this->getTypes() as $type) {
                $input = $type->findAndRedact($input);
            }
        }

        return $input;
    }

    /**
     * @return \Smorken\Redactor\Contracts\Types\Base[]
     */
    public function getTypes(): iterable
    {
        return $this->types;
    }

    public function setTypes(array $types): void
    {
        $this->types = [];
        foreach ($types as $type) {
            $this->addType($type);
        }
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): void
    {
        $this->active = $active;
    }
}
