<?php

namespace Smorken\Redactor\Exceptions;

class WithEncryptException extends \Exception {}
