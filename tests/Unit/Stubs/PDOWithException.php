<?php

namespace Tests\Smorken\Redactor\Unit\Stubs;

class PDOWithException extends \PDO
{
    public function __construct($dsn, $username = null, $password = null, $options = null)
    {
        throw new \Exception('Test PDO Exception');
        //parent::__construct($dsn, $username, $password, $options);
    }
}
