<?php

namespace Tests\Smorken\Redactor\Unit\Stubs;

class Ext extends Base
{
    public $pub = 3;

    protected $prot = 2;

    protected $array_data = [
        'password' => '1',
        'other' => '2',
    ];

    public function getArrayData()
    {
        return $this->array_data;
    }
}
