<?php

namespace Tests\Smorken\Redactor\Unit\Stubs;

class Base
{
    public static $pubstat = 10;

    public $pub = 2;

    protected $prot = 1;

    private $priv = 0;

    public function __construct()
    {
        self::$pubstat = 10;
    }

    public function getPriv()
    {
        return $this->priv;
    }

    public function setPriv($priv)
    {
        $this->priv = $priv;
    }

    public function getProt()
    {
        return $this->prot;
    }

    public function setProt($prot)
    {
        $this->prot = $prot;
    }
}
