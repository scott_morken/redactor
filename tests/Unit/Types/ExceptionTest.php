<?php

namespace Tests\Smorken\Redactor\Unit\Types;

use PHPUnit\Framework\TestCase;
use Smorken\Redactor\Types\Exception;

function test($username, $password)
{
    throw new \Exception('Error testing function!');
}

class ExceptionTest extends TestCase
{
    public function testFunctionException()
    {
        $sut = new Exception('test', [1]);
        try {
            test('foo', 'bar');
        } catch (\Exception $e) {
            $r = $sut->findAndRedact($e);
        }
        $expected = "Tests\Smorken\Redactor\Unit\Types\\test('foo', '[ REDACTED ]')";
        $not = "Tests\Smorken\Redactor\Unit\Types\\test('foo', 'bar')";
        $this->assertStringContainsString($expected, $r->getTraceAsString());
        $this->assertStringNotContainsString($not, $r->getTraceAsString());
    }

    public function testMethodExceptionMultipleRedactors()
    {
        $suts = [
            new Exception('Test::test', [1]),
            new Exception('test', [1]),
        ];
        try {
            $t = new Test;
            $t->test('foo', 'bar');
        } catch (\Exception $e) {
            foreach ($suts as $sut) {
                $r = $sut->findAndRedact($e);
            }
        }
        $expected[] = "Tests\Smorken\Redactor\Unit\Types\\test('foo', '[ REDACTED ]')";
        $not[] = "Tests\Smorken\Redactor\Unit\Types\\test('foo', 'bar')";
        $expected[] = "Tests\Smorken\Redactor\Unit\Types\Test->test('foo', '[ REDACTED ]')";
        $not[] = "Tests\Smorken\Redactor\Unit\Types\Test->test('foo', 'bar')";
        foreach ($not as $n) {
            $this->assertStringNotContainsString($n, $r->getTraceAsString());
        }
        foreach ($expected as $e) {
            $this->assertStringContainsString($e, $r->getTraceAsString());
        }
    }

    public function testMethodExceptionOneRedactor()
    {
        $sut = new Exception('Test::test', [1]);
        try {
            $t = new Test;
            $t->test('foo', 'bar');
        } catch (\Exception $e) {
            $r = $sut->findAndRedact($e);
        }
        $expected[] = "Tests\Smorken\Redactor\Unit\Types\\test('foo', 'bar')";
        $not[] = "Tests\Smorken\Redactor\Unit\Types\\test('foo', '[ REDACTED ]')";
        $expected[] = "Tests\Smorken\Redactor\Unit\Types\Test->test('foo', '[ REDACTED ]')";
        $not[] = "Tests\Smorken\Redactor\Unit\Types\Test->test('foo', 'bar')";
        foreach ($not as $n) {
            $this->assertStringNotContainsString($n, $r->getTraceAsString());
        }
        foreach ($expected as $e) {
            $this->assertStringContainsString($e, $r->getTraceAsString());
        }
    }

    public function testMethodExceptionRedactorWithAnyClass()
    {
        $sut = new Exception('*::test', [1]);
        try {
            $t = new Test;
            $t->test('foo', 'bar');
        } catch (\Exception $e) {
            $r = $sut->findAndRedact($e);
        }
        $expected[] = "Tests\Smorken\Redactor\Unit\Types\\test('foo', '[ REDACTED ]')";
        $not[] = "Tests\Smorken\Redactor\Unit\Types\\test('foo', 'bar')";
        $expected[] = "Tests\Smorken\Redactor\Unit\Types\Test->test('foo', '[ REDACTED ]')";
        $not[] = "Tests\Smorken\Redactor\Unit\Types\Test->test('foo', 'bar')";
        foreach ($not as $n) {
            $this->assertStringNotContainsString($n, $r->getTraceAsString());
        }
        foreach ($expected as $e) {
            $this->assertStringContainsString($e, $r->getTraceAsString());
        }
    }

    public function testMethodExceptionRedactorWithWrongStrlenClass()
    {
        $sut = new Exception('TestConstr::__construct', [1]);
        try {
            $t = new Test;
            $t->test('foo', 'bar');
        } catch (\Exception $e) {
            $r = $sut->findAndRedact($e);
        }
        $expected[] = "Tests\Smorken\Redactor\Unit\Types\\test('foo', 'bar')";
        $not[] = "Tests\Smorken\Redactor\Unit\Types\\test('foo', '[ REDACTED ]')";
        $expected[] = "Tests\Smorken\Redactor\Unit\Types\Test->test('foo', 'bar')";
        $not[] = "Tests\Smorken\Redactor\Unit\Types\Test->test('foo', '[ REDACTED ]')";
        foreach ($not as $n) {
            $this->assertStringNotContainsString($n, $r->getTraceAsString());
        }
        foreach ($expected as $e) {
            $this->assertStringContainsString($e, $r->getTraceAsString());
        }
    }
}

class Test
{
    public function test($username, $password)
    {
        test($username, $password);
    }
}

class TestConstr
{
    public function __construct()
    {
        throw new \Exception('Bad Constructor');
    }
}
