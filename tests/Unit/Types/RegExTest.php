<?php

namespace Tests\Smorken\Redactor\Unit\Types;

use PHPUnit\Framework\TestCase;
use Smorken\Redactor\Types\RegEx;
use Tests\Smorken\Redactor\Unit\Stubs\Base;
use Tests\Smorken\Redactor\Unit\Stubs\Ext;

class RegExTest extends TestCase
{
    public function testMatchMultiDimArrayComplex()
    {
        $array = [
            'password' => 'foo bar',
            'other_key' => '12345',
            'fiz_buzz' => 'hello',
            'sub_array' => [
                'password' => 'fiz buz',
                'sub_array' => [
                    'password' => [
                        'foo' => 'bar',
                        'fiz_buzz' => 'blah blah',
                    ],
                ],
            ],
        ];
        $expected = [
            'password' => '[ REDACTED ]',
            'other_key' => '12345',
            'fiz_buzz' => '[ REDACTED ]',
            'sub_array' => [
                'password' => '[ REDACTED ]',
                'sub_array' => [
                    'password' => '[ REDACTED ]',
                ],
            ],
        ];
        $sut = new RegEx(['/password/', '/fiz_buzz/']);
        $this->assertEquals($expected, $sut->findAndRedact($array));
        $this->assertTrue($sut->redacted());
    }

    public function testMatchMultipleArrayKey()
    {
        $array = [
            'password' => 'foo bar',
            'other_key' => '12345',
            'fiz_buzz' => 'hello',
        ];
        $expected = [
            'password' => '[ REDACTED ]',
            'other_key' => '12345',
            'fiz_buzz' => '[ REDACTED ]',
        ];
        $sut = new RegEx(['/password/', '/fiz_buzz/']);
        $this->assertEquals($expected, $sut->findAndRedact($array));
        $this->assertTrue($sut->redacted());
    }

    public function testMatchMultiplePubPrivInObject()
    {
        $o = new Base;
        $sut = new RegEx(['/pub/', '/priv/']);
        $r = $sut->findAndRedact($o);
        $this->assertEquals('[ REDACTED ]', $r->pub);
        $this->assertEquals('[ REDACTED ]', Base::$pubstat);
        $this->assertEquals(1, $r->getProt());
        $this->assertEquals('[ REDACTED ]', $r->getPriv());
        $this->assertTrue($sut->redacted());
    }

    public function testMatchNoneInObject()
    {
        $o = new Base;
        $sut = new RegEx(['/fiz/', '/buz/']);
        $r = $sut->findAndRedact($o);
        $this->assertEquals(10, Base::$pubstat);
        $this->assertEquals(2, $r->pub);
        $this->assertEquals(1, $r->getProt());
        $this->assertEquals(0, $r->getPriv());
        $this->assertFalse($sut->redacted());
    }

    public function testMatchProtArrayItemInExtObject()
    {
        $o = new Ext;
        $sut = new RegEx(['/pass[A-z]*/']);
        $r = $sut->findAndRedact($o);
        $this->assertEquals(10, Ext::$pubstat);
        $this->assertEquals(3, $r->pub);
        $this->assertEquals(2, $r->getProt());
        $this->assertEquals(0, $r->getPriv());
        $this->assertEquals([
            'password' => '[ REDACTED ]',
            'other' => '2',
        ], $r->getArrayData());
        $this->assertTrue($sut->redacted());
    }

    public function testMatchSingleArrayKey()
    {
        $array = [
            'password' => 'foo bar',
            'other_key' => '12345',
            'fiz_buzz' => 'hello',
        ];
        $expected = [
            'password' => '[ REDACTED ]',
            'other_key' => '12345',
            'fiz_buzz' => 'hello',
        ];
        $sut = new RegEx(['/passw.*d/']);
        $this->assertEquals($expected, $sut->findAndRedact($array));
        $this->assertTrue($sut->redacted());
    }

    public function testMatchSingleArrayValue()
    {
        $array = [
            'authentication' => '{ user: foo, password: 12345 }',
            'other_key' => '12345',
            'fiz_buzz' => 'hello',
        ];
        $expected = [
            'authentication' => '{ user: foo, [ REDACTED ] }',
            'other_key' => '12345',
            'fiz_buzz' => 'hello',
        ];
        $sut = new RegEx(['/passw.*d\s*[\=\:\>]+\s*\w*/']);
        $this->assertEquals($expected, $sut->findAndRedact($array));
        $this->assertTrue($sut->redacted());
    }

    public function testMatchSinglePrivInExtObject()
    {
        $o = new Ext;
        $sut = new RegEx(['/priv/']);
        $r = $sut->findAndRedact($o);
        $this->assertEquals(10, Ext::$pubstat);
        $this->assertEquals(3, $r->pub);
        $this->assertEquals(2, $r->getProt());
        $this->assertEquals('[ REDACTED ]', $r->getPriv());
        $this->assertEquals([
            'password' => '1',
            'other' => '2',
        ], $r->getArrayData());
        $this->assertTrue($sut->redacted());
    }

    public function testMatchSingleProtInExtObject()
    {
        $o = new Ext;
        $sut = new RegEx(['/prot/']);
        $r = $sut->findAndRedact($o);
        $this->assertEquals(10, Ext::$pubstat);
        $this->assertEquals(3, $r->pub);
        $this->assertEquals('[ REDACTED ]', $r->getProt());
        $this->assertEquals(0, $r->getPriv());
        $this->assertEquals([
            'password' => '1',
            'other' => '2',
        ], $r->getArrayData());
        $this->assertTrue($sut->redacted());
    }

    public function testMatchSinglePublicInObject()
    {
        $o = new Base;
        $sut = new RegEx(['/pub/']);
        $r = $sut->findAndRedact($o);
        $this->assertEquals('[ REDACTED ]', $r->pub);
        $this->assertEquals('[ REDACTED ]', Base::$pubstat);
        $this->assertEquals(1, $r->getProt());
        $this->assertEquals(0, $r->getPriv());
        $this->assertTrue($sut->redacted());
    }

    public function testNoMatchInArray()
    {
        $array = [
            'password' => 'foo bar',
            'other_key' => '12345',
            'fiz_buzz' => 'hello',
        ];
        $sut = new RegEx(['/passwd/']);
        $this->assertEquals($array, $sut->findAndRedact($array));
        $this->assertFalse($sut->redacted());
    }
}
