<?php

namespace Tests\Smorken\Redactor\Unit\Types;

use PHPUnit\Framework\TestCase;
use Smorken\Redactor\Types\MethodOrFunction;

class MethodOrFunctionTest extends TestCase
{
    public function testSimpleStringMatch()
    {
        $str = "bind('username', 'password')";
        $expected = "bind('username', [ REDACTED ])";
        $sut = new MethodOrFunction('bind', [1]);
        $this->assertEquals($expected, $sut->findAndRedact($str));
        $this->assertTrue($sut->redacted());
    }

    public function testMultipleStringMatch()
    {
        $str = "bind('username', 'password'); bind('username1', 'password1')";
        $expected = "bind('username', [ REDACTED ]); bind('username1', [ REDACTED ])";
        $sut = new MethodOrFunction('bind', [1]);
        $this->assertEquals($expected, $sut->findAndRedact($str));
        $this->assertTrue($sut->redacted());
    }

    public function testNestedFunctionMatchInternal()
    {
        $str = "authenticate(bind('username', 'password'))";
        $expected = "authenticate(bind('username', [ REDACTED ]))";
        $sut = new MethodOrFunction('bind', [1]);
        $this->assertEquals($expected, $sut->findAndRedact($str));
        $this->assertTrue($sut->redacted());
    }

    public function testNestedFunctionMatchExternal()
    {
        $str = "bind(lookup('username'), 'password')";
        $expected = "bind(lookup('username'), [ REDACTED ])";
        $sut = new MethodOrFunction('bind', [1]);
        $this->assertEquals($expected, $sut->findAndRedact($str));
        $this->assertTrue($sut->redacted());
    }
}
