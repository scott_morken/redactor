<?php

namespace Tests\Smorken\Redactor\Unit;

use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Redactor\Contracts\Redactor;
use Smorken\Redactor\Factory;
use Smorken\Redactor\Types\RegEx;
use Smorken\Redactor\Withs\SimpleString;

class FactoryTest extends TestCase
{
    public function testFactoryWithReflectionExceptionAndActiveSkipsTypes()
    {
        $config = [
            'active' => true,
            'types' => [
                'InvalidClassName' => [
                    [
                        'param1' => 'foo',
                    ],
                    [
                        'param1' => 'bar',
                    ],
                ],
            ],
        ];
        $sut = Factory::fromConfig($config);
        $this->assertInstanceOf(Redactor::class, $sut);
        $this->assertEmpty($sut->getTypes());
    }

    public function testFactoryWithReflectionExceptionAndNotActiveIsException()
    {
        $config = [
            'active' => false,
            'types' => [
                'InvalidClassName' => [
                    [
                        'param1' => 'foo',
                    ],
                    [
                        'param1' => 'bar',
                    ],
                ],
            ],
        ];
        $this->expectException(\ReflectionException::class);
        $sut = Factory::fromConfig($config);
    }

    public function testFactoryWithMixedResultsAndActiveSkipsInvalidTypes()
    {
        $config = [
            'active' => true,
            'types' => [
                'InvalidClassName' => [
                    [
                        'param1' => 'foo',
                    ],
                    [
                        'param1' => 'bar',
                    ],
                ],
                'AnotherInvalidNameWithValidType' => [
                    m::mock(\Smorken\Redactor\Contracts\Types\SimpleString::class),
                ],
                RegEx::class => [
                    [
                        ['abc', '123'],
                    ],
                ],
            ],
        ];
        $sut = Factory::fromConfig($config);
        $this->assertInstanceOf(Redactor::class, $sut);
        $this->assertCount(2, $sut->getTypes());
    }

    public function testFactoryWithMultipleOfSameType()
    {
        $config = [
            'active' => true,
            'types' => [
                RegEx::class => [
                    [
                        ['abc', '123'],
                    ],
                    [
                        ['def'], new SimpleString('****'),
                    ],
                    [
                        ['foo'],
                    ],
                ],
            ],
        ];
        $sut = Factory::fromConfig($config);
        $this->assertInstanceOf(Redactor::class, $sut);
        $this->assertCount(3, $sut->getTypes());
    }
}
