<?php

namespace Tests\Smorken\Redactor\Unit\Redactor;

use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Redactor\Contracts\Types\MethodOrFunction;
use Smorken\Redactor\Contracts\Types\RegEx;
use Smorken\Redactor\Contracts\Types\SimpleString;
use Smorken\Redactor\Redactor;

class InstantiationTest extends TestCase
{
    public function testActiveDefaultsToTrue()
    {
        $sut = new Redactor([]);
        $this->assertTrue($sut->isActive());
    }

    public function testWithInvalidTypeIsException()
    {
        $types = [
            new \stdClass,
        ];
        $this->expectException(\TypeError::class);
        $sut = new Redactor($types, false);
    }

    public function testWithMixedTypesIsException()
    {
        $bad = new \stdClass;
        $types = [
            $bad,
            m::mock(SimpleString::class),
            m::mock(MethodOrFunction::class),
            m::mock(RegEx::class),
        ];
        $this->expectException(\TypeError::class);
        $sut = new Redactor($types);
    }

    public function testWithMixedTypes()
    {
        $types = [
            m::mock(SimpleString::class),
            m::mock(MethodOrFunction::class),
            m::mock(RegEx::class),
        ];
        $sut = new Redactor($types);
        $this->assertCount(3, $sut->getTypes());
    }
}
