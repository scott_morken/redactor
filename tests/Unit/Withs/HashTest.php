<?php

namespace Tests\Smorken\Redactor\Unit\Withs;

use PHPUnit\Framework\TestCase;
use Smorken\Redactor\Exceptions\WithEncryptException;
use Smorken\Redactor\Withs\Hash;

class HashTest extends TestCase
{
    public function testHashAndRehashMatch()
    {
        $sut = new Hash('1234567812345678');
        $hash1 = $sut->redact('foo');
        $hash2 = $sut->redact('foo');
        $this->assertTrue(hash_equals($hash1, $hash2));
    }

    public function testHashInvalidKeyLengthIsException()
    {
        $this->expectException(WithEncryptException::class);
        $sut = new Hash('12345');
        $hash = $sut->redact('foo');
    }

    public function testNoSaltAutogeneratesRandom()
    {
        $sut1 = new Hash('');
        $hash1 = $sut1->redact('foo');
        $sut2 = new Hash('');
        $hash2 = $sut2->redact('foo');
        $this->assertNotEquals($hash2, $hash1);
    }
}
