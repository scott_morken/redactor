<?php

namespace Tests\Smorken\Redactor\Unit\Withs;

use PHPUnit\Framework\TestCase;
use Smorken\Redactor\Exceptions\WithEncryptException;
use Smorken\Redactor\Utils\Random;
use Smorken\Redactor\Withs\Encrypt;

class EncryptTest extends TestCase
{
    public function testDecryptIsExceptionWhenNotDebug()
    {
        $sut = new Encrypt('');
        $key = $sut->getKey();
        $cipher = $sut->redact('This is a really good test.');
        $this->expectException(WithEncryptException::class);
        $this->expectExceptionMessage('Debug disabled');
        $sut->decrypt($cipher, $key);
    }

    public function testDecryptIsExceptionWhenCipherChanged()
    {
        $sut = new Encrypt('', 16, true);
        $key = $sut->getKey();
        $cipher = $sut->redact('This is a really good test.');
        $cipher = '1a23'.substr($cipher, 4);
        $this->expectException(WithEncryptException::class);
        $this->expectExceptionMessage('The message was tampered with');
        $sut->decrypt($cipher, $key);
    }

    public function testEncryptAndDecryptWithSelfGeneratedKey()
    {
        $sut = new Encrypt('', 16, true);
        $key = $sut->getKey();
        $cipher = $sut->redact('This is a really good test.');
        $this->assertEquals('This is a really good test.', $sut->decrypt($cipher, $key));
    }

    public function testEncryptAndDecryptWithDifferentKeyIsException()
    {
        $key = Random::binary();
        $sut = new Encrypt($key, 16, true);
        $cipher = $sut->redact('This is a really good test.');
        $key = Random::binary();
        $this->expectException(WithEncryptException::class);
        $this->expectExceptionMessage('The message was tampered with');
        $sut->decrypt($cipher, $key);
    }

    public function testEncryptAndDecryptWithSetKey()
    {
        $key = Random::binary();
        $sut = new Encrypt($key, 16, true);
        $cipher = $sut->redact('This is a really good test.');
        $this->assertEquals('This is a really good test.', $sut->decrypt($cipher, $key));
    }

    public function testEncryptInvalidKeyLength()
    {
        $sut = new Encrypt('abc123', 16, true);
        $this->expectException(WithEncryptException::class);
        $this->expectExceptionMessage('Key must be');
        $cipher = $sut->redact('This is a really good test.');
    }
}
