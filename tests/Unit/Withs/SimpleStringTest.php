<?php

namespace Tests\Smorken\Redactor\Unit\Withs;

use PHPUnit\Framework\TestCase;
use Smorken\Redactor\Withs\SimpleString;

class SimpleStringTest extends TestCase
{
    public function testCanOverrideDefault()
    {
        $sut = new SimpleString('FOO BAR');
        $this->assertEquals('FOO BAR', $sut->redact('password'));
    }

    public function testWithString()
    {
        $str = 'password123';
        $this->assertEquals('[ REDACTED ]', (new SimpleString)->redact($str));
    }
}
