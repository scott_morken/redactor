<?php

namespace Tests\Smorken\Redactor\Integration;

use PHPUnit\Framework\TestCase;
use Smorken\Redactor\Factory;
use Smorken\Redactor\Types\Exception;
use Smorken\Redactor\Types\MethodOrFunction;
use Smorken\Redactor\Types\RegEx;
use Smorken\Redactor\Types\SimpleString;
use Tests\Smorken\Redactor\Unit\Stubs\PDOWithException;

class PDORedactorTest extends TestCase
{
    public function testPDOContructorException(): void
    {
        $sut = $this->getSut([
            'active' => true,
            'types' => [
                Exception::class => [
                    [
                        'PDOWithException::__construct',
                        [0, 2],
                    ],
                ],
            ],
        ]);
        $expected = <<<DOC
Exception: Test PDO Exception in /app/tests/Unit/Stubs/PDOWithException.php:9
Stack trace:
#0 /app/tests/Integration/PDORedactorTest.php(35): Tests\Smorken\Redactor\Unit\Stubs\PDOWithException->__construct('[ REDACTED ]', 'username', '[ REDACTED ]', Array)
DOC;

        try {
            $pdo = new PDOWithException('sqlsrv:dsn', 'username', 'password', []);
        } catch (\Throwable $e) {
            $redacted = $sut->findAndRedact($e);
        }
        $this->assertStringStartsWith($expected, (string) $redacted);
    }

    public function testPDOStackTrace()
    {
        $str = file_get_contents(__DIR__.'/../data/PDO_stack_trace.txt');
        $expected = <<<DOC
PDOException: SQLSTATE[HY000] [2002] Connection refused in /www/ssk/prod/releases/[ REDACTED ]/vendor/laravel/framework/src/Illuminate/Database/Connectors/Connector.php:67
Stack trace:
#0 /www/ssk/prod/releases/[ REDACTED ]/vendor/laravel/framework/src/Illuminate/Database/Connectors/Connector.php(67): PDO->__construct([ REDACTED ], 'username', [ REDACTED ], Array)
#1 /www/ssk/prod/releases/[ REDACTED ]/vendor/laravel/framework/src/Illuminate/Database/Connectors/Connector.php(43): Illuminate\Database\Connectors\Connector->createPdoConnection([ REDACTED ], 'username', [ REDACTED ], Array)
#2 /www/ssk/prod/releases/[ REDACTED ]/vendor/laravel/framework/src/Illuminate/Database/Connectors/MySqlConnector.php(24): Illuminate\Database\Connectors\Connector->createConnection('mysql:host=192....', Array, Array)
#3 /www/ssk/prod/releases/[ REDACTED ]/vendor/laravel/framework/src/Illuminate/Database/Connectors/ConnectionFactory.php(183): Illuminate\Database\Connectors\MySqlConnector->connect(Array)
#4 [internal function]: Illuminate\Database\Connectors\ConnectionFactory->Illuminate\Database\Connectors\{closure}()

DOC;
        $sut = $this->getSut();
        $redacted = $sut->findAndRedact($str);
        $this->assertEquals($expected, $redacted);
    }

    public function testPDOStackTraceAddType()
    {
        $str = file_get_contents(__DIR__.'/../data/PDO_stack_trace.txt');
        $expected = <<<DOC
PDOException: SQLSTATE[HY000] [2002] Connection refused in /www/ssk/prod/releases/[ REDACTED ]/vendor/laravel/framework/src/Illuminate/Database/Connectors/Connector.php:67
Stack trace:
#0 /www/ssk/prod/releases/[ REDACTED ]/vendor/laravel/framework/src/Illuminate/Database/Connectors/Connector.php(67): PDO->__construct([ REDACTED ], 'username', [ REDACTED ], Array)
#1 /www/ssk/prod/releases/[ REDACTED ]/vendor/laravel/framework/src/Illuminate/Database/Connectors/Connector.php(43): Illuminate\Database\Connectors\Connector->createPdoConnection([ REDACTED ], 'username', [ REDACTED ], Array)
#2 /www/ssk/prod/releases/[ REDACTED ]/vendor/laravel/framework/src/Illuminate/Database/Connectors/MySqlConnector.php(24): Illuminate\Database\Connectors\Connector->createConnection('mysql:host=192....', Array, [ REDACTED ])
#3 /www/ssk/prod/releases/[ REDACTED ]/vendor/laravel/framework/src/Illuminate/Database/Connectors/ConnectionFactory.php(183): Illuminate\Database\Connectors\MySqlConnector->connect(Array)
#4 [internal function]: Illuminate\Database\Connectors\ConnectionFactory->Illuminate\Database\Connectors\{closure}()

DOC;
        $sut = $this->getSut();
        $sut->addType(new MethodOrFunction('createConnection', [2]));
        $sut->addType(new MethodOrFunction('connect', [1])); //doesn't exist
        $redacted = $sut->findAndRedact($str);
        $this->assertEquals($expected, $redacted);
    }

    protected function getConfig()
    {
        return [
            'active' => true,
            'types' => [
                SimpleString::class => [
                    [
                        ['password'],
                    ],
                ],
                RegEx::class => [
                    [
                        ['/[0-9]{12}/'],
                    ],
                ],
                MethodOrFunction::class => [
                    [
                        'PDO->__construct',
                        [0, 2],
                    ],
                    [
                        'createPdoConnection',
                        [0, 2],
                    ],
                ],
            ],
        ];
    }

    /**
     * @param  null  $config
     * @return \Smorken\Redactor\Contracts\Redactor
     *
     * @throws \Exception
     */
    protected function getSut($config = null)
    {
        if (is_null($config)) {
            $config = $this->getConfig();
        }
        $sut = Factory::fromConfig($config);

        return $sut;
    }
}
