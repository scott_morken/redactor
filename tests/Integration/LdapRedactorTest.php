<?php

namespace Tests\Smorken\Redactor\Integration;

use PHPUnit\Framework\TestCase;
use Smorken\Redactor\Factory;
use Smorken\Redactor\Types\MethodOrFunction;
use Smorken\Redactor\Types\RegEx;
use Smorken\Redactor\Types\SimpleString;

class LdapRedactorTest extends TestCase
{
    public function testLdapStackTrace()
    {
        $str = file_get_contents(__DIR__.'/../data/LDAP_stack_trace.txt');
        $expected = <<<DOC
LdapQuery\Clients\ClientException: Can't contact LDAP server in /www/auth-proxy/prod/releases/[ REDACTED ]/vendor/smorken/ldapquery/src/Clients/Ldap.php:81
Stack trace:
#0 /www/auth-proxy/prod/releases/[ REDACTED ]/vendor/smorken/ldapquery/src/Backends/Base.php(51): LdapQuery\Clients\Ldap->bind('username@mccc...', [ REDACTED ])
#1 /www/auth-proxy/prod/releases/[ REDACTED ]/vendor/smorken/ldapquery/src/LdapQuery.php(90): LdapQuery\Backends\Base->bind('username@mccc...', [ REDACTED ])
#2 /www/auth-proxy/prod/releases/[ REDACTED ]/app/Authenticate/Proxies/ActiveDirectory.php(187): LdapQuery\LdapQuery->authenticate('username', [ REDACTED ])
#3 /www/auth-proxy/prod/releases/[ REDACTED ]/app/Authenticate/Proxies/ActiveDirectory.php(74): App\Authenticate\Proxies\ActiveDirectory->doAuthentication('username', 'secret')
#4 /www/auth-proxy/prod/releases/[ REDACTED ]/app/Http/Controllers/Authenticate/Controller.php(123): App\Authenticate\Proxies\ActiveDirectory->authenticate('username', [ REDACTED ])
#5 /www/auth-proxy/prod/releases/[ REDACTED ]/app/Http/Controllers/Authenticate/Controller.php(74): App\Http\Controllers\Authenticate\Controller->attemptAuthenticate(Object(Illuminate\Http\Request), Object(App\Models\Eloquent\Token))

DOC;
        $sut = $this->getSut();
        $redacted = $sut->findAndRedact($str);
        $this->assertEquals($expected, $redacted);
    }

    public function testLdapStackTraceAddType()
    {
        $str = file_get_contents(__DIR__.'/../data/LDAP_stack_trace.txt');
        $expected = <<<DOC
LdapQuery\Clients\ClientException: Can't contact LDAP server in /www/auth-proxy/prod/releases/[ REDACTED ]/vendor/smorken/ldapquery/src/Clients/Ldap.php:81
Stack trace:
#0 /www/auth-proxy/prod/releases/[ REDACTED ]/vendor/smorken/ldapquery/src/Backends/Base.php(51): LdapQuery\Clients\Ldap->bind('username@mccc...', [ REDACTED ])
#1 /www/auth-proxy/prod/releases/[ REDACTED ]/vendor/smorken/ldapquery/src/LdapQuery.php(90): LdapQuery\Backends\Base->bind('username@mccc...', [ REDACTED ])
#2 /www/auth-proxy/prod/releases/[ REDACTED ]/app/Authenticate/Proxies/ActiveDirectory.php(187): LdapQuery\LdapQuery->authenticate('username', [ REDACTED ])
#3 /www/auth-proxy/prod/releases/[ REDACTED ]/app/Authenticate/Proxies/ActiveDirectory.php(74): App\Authenticate\Proxies\ActiveDirectory->doAuthentication('username', [ REDACTED ])
#4 /www/auth-proxy/prod/releases/[ REDACTED ]/app/Http/Controllers/Authenticate/Controller.php(123): App\Authenticate\Proxies\ActiveDirectory->authenticate('username', [ REDACTED ])
#5 /www/auth-proxy/prod/releases/[ REDACTED ]/app/Http/Controllers/Authenticate/Controller.php(74): App\Http\Controllers\Authenticate\Controller->attemptAuthenticate(Object(Illuminate\Http\Request), Object(App\Models\Eloquent\Token))

DOC;
        $sut = $this->getSut();
        $sut->addType(new MethodOrFunction('doAuthentication', [1]));
        $redacted = $sut->findAndRedact($str);
        $this->assertEquals($expected, $redacted);
    }

    protected function getConfig()
    {
        return [
            'active' => true,
            'types' => [
                SimpleString::class => [
                    [
                        ['password'],
                    ],
                ],
                RegEx::class => [
                    [
                        ['/[0-9]{12}/'],
                    ],
                ],
                MethodOrFunction::class => [
                    [
                        'bind',
                        [1],
                    ],
                    [
                        'authenticate',
                        [1],
                    ],
                ],
            ],
        ];
    }

    /**
     * @param  null  $config
     * @return \Smorken\Redactor\Contracts\Redactor
     *
     * @throws \Exception
     */
    protected function getSut($config = null)
    {
        if (is_null($config)) {
            $config = $this->getConfig();
        }
        $sut = Factory::fromConfig($config);

        return $sut;
    }
}
