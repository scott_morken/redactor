<?php

namespace Tests\Smorken\Redactor\Integration\Log\Monolog;

use Monolog\Handler\TestHandler;
use Monolog\Level;
use Monolog\LogRecord;
use PHPUnit\Framework\TestCase;
use Smorken\Redactor\Factory;
use Smorken\Redactor\Log\Monolog\RedactorProcessor;
use Smorken\Redactor\Types\MethodOrFunction;
use Smorken\Redactor\Types\RegEx;
use Smorken\Redactor\Types\SimpleString;

class RedactorProcessorTest extends TestCase
{
    public function getHandler()
    {
        $processor = new RedactorProcessor(Factory::fromConfig($this->getConfig()));
        $handler = new TestHandler;
        $handler->pushProcessor($processor);

        return $handler;
    }

    public function testHandlerWithRecordNoRedactions()
    {
        $handler = $this->getHandler();
        $record = $this->getRecord();
        $handler->handle($record);
        $expected = [
            'message' => 'test',
            'context' => [],
            'extra' => [],
        ];
        foreach ($expected as $key => $value) {
            $this->assertEquals($value, $record[$key]);
        }
    }

    public function testHandlerWithRecordRedactions()
    {
        $handler = $this->getHandler();
        $data = ['username' => 'foo', 'password' => 'fiz buz'];
        $record = $this->getRecord(Level::Warning, 'test', $data);
        $record['extra']['json'] = json_encode($data);
        $handler->handle($record);
        [$handled_record] = $handler->getRecords();
        $expected = 'test.WARNING: test {"username":"foo","password":"[ REDACTED ]"} {"json":"{\"username\":\"foo\",[ REDACTED ]}"}'.PHP_EOL;
        $this->assertStringEndsWith($expected, $handled_record['formatted']);
    }

    protected function getConfig()
    {
        return [
            'active' => true,
            'types' => [
                SimpleString::class => [
                    [
                        ['password'],
                    ],
                ],
                RegEx::class => [
                    [
                        ['/"pass(.*?)"\s*\:\s*"(.*?)"/'],
                    ],
                ],
                MethodOrFunction::class => [
                    [
                        'bind',
                        [1],
                    ],
                    [
                        'authenticate',
                        [1],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    protected function getMultipleRecords()
    {
        return [
            $this->getRecord(Level::Debug, 'debug message 1'),
            $this->getRecord(Level::Debug, 'debug message 2'),
            $this->getRecord(Level::Info, 'information'),
            $this->getRecord(Level::Warning, 'warning'),
            $this->getRecord(Level::Error, 'error'),
        ];
    }

    /**
     * @return \Monolog\LogRecord Record
     */
    protected function getRecord($level = Level::Warning, $message = 'test', $context = [])
    {
        $params = [
            'message' => $message,
            'context' => $context,
            'level' => $level,
            'channel' => 'test',
            'datetime' => \DateTimeImmutable::createFromFormat('U.u', sprintf('%.6F', microtime(true))),
            'extra' => [],
        ];

        return new LogRecord(...$params);
    }
}
