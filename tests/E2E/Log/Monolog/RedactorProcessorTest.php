<?php

namespace Tests\Smorken\Redactor\E2E\Log\Monolog;

use Monolog\Handler\TestHandler;
use Monolog\Logger;
use PHPUnit\Framework\TestCase;
use Smorken\Redactor\Factory;
use Smorken\Redactor\Log\Monolog\RedactorProcessor;
use Smorken\Redactor\Types\Exception;
use Smorken\Redactor\Types\MethodOrFunction;
use Smorken\Redactor\Types\RegEx;

class RedactorProcessorTest extends TestCase
{
    public function testProcessorNoRedaction()
    {
        [$logger, $handler] = $this->getLogger();
        $logger->error('test');
        [$record] = $handler->getRecords();
        $this->assertEquals('test', $record['message']);
        $this->assertEquals([], $record['context']);
        $this->assertEquals([], $record['extra']);
    }

    public function testProcessorRedactArrayKeyValue()
    {
        [$logger, $handler] = $this->getLogger();
        $logger->error('test', ['username' => 'foo', 'password' => 'bar']);
        [$record] = $handler->getRecords();
        $this->assertEquals('test', $record['message']);
        $this->assertEquals(['username' => 'foo', 'password' => '[ REDACTED ]'], $record['context']);
        $this->assertEquals([], $record['extra']);
    }

    public function testProcessorRedactExceptionWithStackTrace()
    {
        [$logger, $handler] = $this->getLogger();
        try {
            $t = new Test;
            $t->authenticate('foo', 'bar');
        } catch (\Exception $e) {
            $logger->error($e->getMessage(), ['exception' => $e]);
        }
        [$record] = $handler->getRecords();
        $this->assertEquals("Couldn't authenticate", $record['message']);
        $this->assertStringContainsString("Tests\Smorken\Redactor\E2E\Log\Monolog\Test->authenticate('foo', '[ REDACTED ]')",
            $record['context']['exception']->getTraceAsString());
        $this->assertEquals([], $record['extra']);
    }

    protected function getConfig()
    {
        return [
            'active' => true,
            'types' => [
                RegEx::class => [
                    [
                        ['/pass(.*)/', '/"pass(.*?)"\s*\:\s*"(.*?)"/'],
                    ],
                ],
                MethodOrFunction::class => [
                    [
                        'bind',
                        [1],
                    ],
                    [
                        'authenticate',
                        [1],
                    ],
                ],
                Exception::class => [
                    [
                        'Test::authenticate',
                        [1],
                    ],
                ],
            ],
        ];
    }

    protected function getLogger($config = null)
    {
        $redactor = $this->getRedactor($config);
        $logger = new Logger(__METHOD__);
        $handler = new TestHandler;
        $logger->pushHandler($handler);
        $processor = new RedactorProcessor($redactor);
        $logger->pushProcessor($processor);

        return [$logger, $handler];
    }

    protected function getRedactor($config = null)
    {
        if (is_null($config)) {
            $config = $this->getConfig();
        }

        return Factory::fromConfig($config);
    }
}

class Test
{
    public function authenticate($username, $password)
    {
        throw new \Exception("Couldn't authenticate");
    }
}
