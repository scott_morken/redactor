### Input Redactor

##### Requirements

* PHP 7.2 (Hash/Encrypt)
* Sodium (Hash/Encrypt)

##### Handlers
Handlers provide access to the underlying parts of the `$input` for the `Type` 
currently processing the `$input`

`\Smorken\Redactor\Handlers\ExceptionHandler` - used to test the stack trace
from an `Exception`

`\Smorken\Redactor\Handlers\IterableHandler` - used to test the items in an iterable

`\Smorken\Redactor\Handlers\ObjectHandler` - used to test the properties of an object

`\Smorken\Redactor\Handlers\ScalarHandler` - used to test a scalar

##### Types
Types represent the high level `findAndRedact` objects. They are
the objects that the `Redactor` uses to pass input around.

`\Smorken\Redactor\Types\Exception` - used to redact an Exception stack trace
```php
$r1 = new \Smorken\Redactor\Types\Exception('ClassName::connect', []);
//*:: matches on any class name (or just a function)
$r2 = new \Smorken\Redactor\Types\Exception('*::authenticate', [1]);
```

`\Smorken\Redactor\Types\MethodOrFunction` - parses string data for a function name and arguments
```php
$r = new \Smorken\Redactor\Types\MethodOrFunction('connect', [1]);
```

`\Smorken\Redactor\Types\RegEx` - parses array keys, object properties or string data for a match
```php
$r = new \Smorken\Redactor\Types\RegEx(['/pass.*/', '/.*key.*/']);
```

`\Smorken\Redactor\Types\SimpleString` - parses array keys, object properties or string data for a match
```php
$r = new \Smorken\Redactor\Types\SimpleString(['password', 'passwd']);
```

##### Withs
Withs provide the redaction of the `$input`.  They are set per `Type`.

`\Smorken\Redactor\Withs\Encrypt` - encrypt an input using sodium

`\Smorken\Redactor\Withs\Hash` - hash an input using sodium

`\Smorken\Redactor\Withs\SimpleString` - replace the input with a string (default)

### Use

##### Factory
```php
use Smorken\Redactor\Types\Exception;
use Smorken\Redactor\Types\MethodOrFunction;
use Smorken\Redactor\Types\RegEx;
$config = [
   'active' => true,
    'types'  => [
        RegEx::class            => [
            [
                ['/pass(.*)/', '/"pass(.*?)"\s*\:\s*"(.*?)"/'],
                ['/.*key.*/'],
                ['/.*salt.*/'],
            ],
        ],
        MethodOrFunction::class => [
            [
                'connect',
                [],
            ],
        ],
        Exception::class        => [
            [
                'PDO::__construct',
                [2],
            ],
            [
                '*::bind',
                [1],
            ],
            [
                '*::setPassword',
                [],
            ],
        ],
    ],
];
$redactor = \Smorken\Redactor\Factory::fromConfig($config);
$redacted = $redactor->findAndRedact('{"username":"foo","password":"bar"}');
$redacted = $redactor->findAndRedact(['username' => 'foo', 'password' => 'bar']);
$obj = new \stdClass();
$obj->pass = 'foo';
$redacted = $redactor->findAndRedact($obj);
try {
  $dbh = new \PDO('mysql:host=localhost;dbname=test', 'user', 'bad_password');
} catch (\Exception $e) {
  $redacted = $redactor->findAndRedact($e);

}
```

##### Manual creation
```php
use Smorken\Redactor\Types\Exception;
use Smorken\Redactor\Types\MethodOrFunction;
use Smorken\Redactor\Types\RegEx;
use Smorken\Redactor\Withs\Hash;

$types = [
  new RegEx(['/pass.*/'], new Hash('some awesome key')),
  new Exception('*::connect', [1]),
  new Exception('*::authenticate', [1]),
];

$redactor = new \Smorken\Redactor\Redactor($types);
```
